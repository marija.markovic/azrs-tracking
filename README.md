# azrs-tracking

Projekat iz predmeta alati za razvoj softvera.

#### Napomena: 
Za demonstraciju alata meld i gitflow je korišćen projekat [Descan](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/07-descan), a za druge alate iskoristila sam projekat [Simple-Paint](https://github.com/marijam19/Simple-Paint) od prošle godine, da ne bismo ostali clanovi tima Descan i ja koji slušamo AZRS primenjivali alate nad istim projektom.
